import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Router, Route, Switch } from 'react-router-dom';

import history from './history';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import reduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';

import { timeline } from './reducers/timeline';
import { notificacao } from './reducers/header';
import App from './App';
import Login from './components/Login';
import Logout from './components/Logout';

import './css/reset.css';
import './css/styles.css';
import './css/login.css';

const reducers = combineReducers({
  timeline,
  notificacao
});

const store = createStore(reducers, applyMiddleware(reduxThunk));

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact={true} path="/" component={Login} />
        <Route path="/logout" component={Logout} />
        <Route path="/timeline/:login?" component={App} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
