import { likeFoto, listaComentario, listaFotos, notifica } from '../actions/actionsCreator';

export function like(fotoId) {
  return (dispatch) => {
    const url = `https://instalura-api.herokuapp.com/api/fotos/${fotoId}/like?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`;
    fetch(url, { method: 'POST' })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Não foi possível dar like');
        }
      })
      .then(liker => {
        dispatch(likeFoto(fotoId, liker));
        return liker;
      })
      .catch(err => console.log(err.message));
  }
}

export function comenta(fotoId, comentario) {
  return (dispatch) => {
    fetch(`https://instalura-api.herokuapp.com/api/fotos/${fotoId}/comment?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, {
      method: 'POST',
      body: JSON.stringify({ texto: comentario }),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Não foi possível comentar');
        }
      })
      .then(novoComentario => {
        dispatch(listaComentario(fotoId, novoComentario));
        return novoComentario;
      })
      .catch(err => console.log(err.message));
  }
}

export function lista(urlPerfil) {
  return (dispatch) => {
    fetch(urlPerfil)
      .then(res => res.json())
      .then(fotos => {
        dispatch(listaFotos(fotos));
        return fotos;
      })
      .catch(err => console.log(`Erro ao buscar as fotos: ${err}`));
  }
}

export function pesquisaUsuario(usuario) {
  return (dispatch) => {
    fetch(`https://instalura-api.herokuapp.com/api/public/fotos/${usuario}`)
      .then(res => res.json())
      .then(fotos => {
        if(fotos.length === 0) {
          dispatch(notifica('Usuário não encontrado!'));
        } else {
          dispatch(notifica('Usuário encontrado!'));
        }

        dispatch(listaFotos(fotos));
        return fotos;
      })
      .catch(err => console.log(err));
  }
}
