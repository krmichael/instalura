import { LIKE, COMENTARIO, LISTAGEM, NOTIFICACAO } from './Types';

export function likeFoto(fotoId, liker) {
  return { type: LIKE, fotoId, liker };
}

export function listaComentario(fotoId, novoComentario) {
  return { type: COMENTARIO, fotoId, novoComentario };
}

export function listaFotos(fotos) {
  return { type: LISTAGEM, fotos };
}

export function notifica(msg) {
  return { type: NOTIFICACAO, msg }
}
