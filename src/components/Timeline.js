import React, { Component } from "react";
import { connect } from 'react-redux';

import FotoItem from "./Foto";
import { like, comenta, lista } from '../logicas/TimelineApi';

class Timeline extends Component {
  constructor(props) {
    super(props);

    this.login = this.props.login;
  }

  load() {
    let urlPerfil;

    if(!this.login) {
      urlPerfil = `https://instalura-api.herokuapp.com/api/fotos?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`;
    } else {
      urlPerfil = `https://instalura-api.herokuapp.com/api/public/fotos/${this.login}`;
    }

    this.props.lista(urlPerfil);
  }

  componentDidMount() {
    this.load();
  }

  componentWillReceiveProps(nextProps) {
    
    if(nextProps.login !== this.login) {
      this.login = nextProps.login;
      this.load();
    }
  }

  render() {
    return (
      <div className="fotos container">
        {this.props.fotos.map(foto => <FotoItem foto={foto} like={this.props.like} comenta={this.props.comenta} key={foto.id}/>)}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { fotos: state.timeline };
}

const mapDispatchToProps = (dispatch) => {
  return {
    like: (fotoId) => dispatch(like(fotoId)),
    comenta: (fotoId, comentario) => dispatch(comenta(fotoId, comentario)),
    lista: (urlPerfil) => dispatch(lista(urlPerfil))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Timeline);
