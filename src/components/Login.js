import React, { Component } from 'react';
import history from '../history';

export default class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      msg: this.props.location.search.length > 0 ?
      'Ooops! Você precisa estar logado para acessar esse recurso' : ''
    }
  }

  envia = (e) => {
    e.preventDefault();

    fetch('https://instalura-api.herokuapp.com/api/public/login', {
      method: 'POST',
      body: JSON.stringify({login:this.login.value, senha:this.senha.value}),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    })
    .then(response => {
      if(response.ok) {
        return response.text();
      } else {
        throw new Error('Não foi possível fazer o login');
      }
    })
    .then(token => {
      localStorage.setItem('auth-token', token);
      history.push('/timeline');
    })
    .catch(error => {
      this.setState({ msg: error.message });
    });
  }

  render() {
    return (
      <div className="login-box">
        <h1 className="login-logo">Instalura</h1>
        <span>{this.state.msg}</span>

        <form onSubmit={this.envia}>
          <input type="text" ref={(input) => this.login = input}/>
          <input type="password" ref={(input) => this.senha = input}/>
          <input type="submit" value="Login"/>
        </form>
      </div>
    );
  }
}
