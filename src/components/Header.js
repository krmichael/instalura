import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { pesquisaUsuario } from '../logicas/TimelineApi';
import { connect } from 'react-redux';

class Header extends Component {
  
  pesquisa = (e) => {
    e.preventDefault();
    this.props.pesquisaUser(this.usuario.value);
  }

  render() {
    return (
      <header className="header container">
        <h1 className="header-logo">
          Instalura
        </h1>

        <form className="header-busca" onSubmit={this.pesquisa}>
          <input ref={(input) => this.usuario = input} type="text" name="search" placeholder="Pesquisa" className="header-busca-campo" />
          <input type="submit" value="Buscar" className="header-busca-submit" />
        </form>


        <nav>
          <ul className="header-nav">
            <li className="header-nav-item">
              <span style={{fontSize:14}}>{this.props.msg}</span>
              <span>♡</span>
            </li>
          </ul>
        </nav>

        {
          localStorage.getItem('auth-token') ? <Link to="/logout">Sair</Link> : null
        }
      </header>
    );
  }
}

const mapStateToProps = (state) => {
  return { msg: state.notificacao }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pesquisaUser: (usuario) => dispatch(pesquisaUsuario(usuario))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
