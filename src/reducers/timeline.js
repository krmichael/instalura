import { List } from 'immutable';
import { LISTAGEM, COMENTARIO, LIKE } from '../actions/Types';

function trocaFoto(lista, fotoId, callback) {
  const fotoEstadoAntigo = lista.find(foto => foto.id === fotoId)
        ,novasPropriedades = callback(fotoEstadoAntigo)

        ,fotoEstadoNovo = Object.assign({}, fotoEstadoAntigo, novasPropriedades)
        ,indiceLista = lista.findIndex(foto => foto.id === fotoId);

  return lista.set(indiceLista, fotoEstadoNovo);
}

export function timeline(state=new List(), action) {

  switch(action.type) {
    case LISTAGEM:
      return new List(action.fotos);

    case COMENTARIO:
      return trocaFoto(state, action.fotoId, (fotoEstadoAntigo) => {
        const novosComentarios = fotoEstadoAntigo.comentarios.concat(action.novoComentario);
        return {comentarios: novosComentarios};
      });

    case LIKE:
      return trocaFoto(state, action.fotoId, (fotoEstadoAntigo) => {
        const likeada = !fotoEstadoAntigo.likeada
              ,liker = action.liker
              ,possivelLiker = fotoEstadoAntigo.likers.find(likerAtual => likerAtual.login === liker.login);

        let novosLikers;

        if (!possivelLiker) {
          novosLikers = fotoEstadoAntigo.likers.concat(liker);
        } else {
          novosLikers = fotoEstadoAntigo.likers.filter(likerAtual => likerAtual.login !== liker.login);
        }

        return { likeada, likers: novosLikers };
      });

    default:
      return state;
  }
}
