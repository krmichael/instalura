import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import Header from './components/Header';
import Timeline from './components/Timeline';


class App extends Component {
  render() {
    let TIMELINE_PUBLIC_USER = this.props.match.params.login;
    
    if(TIMELINE_PUBLIC_USER === undefined && localStorage.getItem('auth-token') === null) {
      return <Redirect to={{
        pathname: '/',
        search: '?msg=Ooops! Você precisa estar logado para acessar esse recurso'
      }}/>
    }

    return (
      <div className="main">
        <Header />
        <Timeline login={TIMELINE_PUBLIC_USER} />
      </div>
    );
  }
}

export default App;
